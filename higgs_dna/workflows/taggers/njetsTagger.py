import awkward as ak


class njetsTagger:

    def __init__(self) -> None:
        pass

    @property
    def name(self) -> str:
        return "njetsTagger"

    # lower priority is better
    # first decimal point is category within tag (in case for untagged)
    @property
    def priority(self) -> int:
        return 20

    def GetCategory(self, ievt: int) -> int:

        evt_njets = self.njets[ievt][0]

        if evt_njets < 4:
            cat = evt_njets
        else:
            cat = 4

        return cat

    def __call__(self, events: ak.Array, diphotons: ak.Array) -> ak.Array:
        """
        We can classify events according to it's y of diphotons.
        """

        self.njets = events.diphotons.n_jets

        nDiphotons = ak.num(
            events.diphotons.n_jets, axis=1
        )  # Number of entries per row. (N diphotons per row)

        ievts_by_dipho = ak.flatten(
            ak.Array([nDipho * [evt_i] for evt_i, nDipho in enumerate(nDiphotons)])
        )

        cat_vals = ak.Array(map(self.GetCategory, ievts_by_dipho))
        cats = ak.unflatten(cat_vals, nDiphotons)  # Back to size of events.
        cats_by_diphoEvt = self.priority + cats

        return (cats_by_diphoEvt, {})
